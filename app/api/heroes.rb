module Heroes
  class API < Grape::API
    version 'v1'
    format :json
    prefix :api

    get 'heroes/*publishers' do
      %w[batman spiderman superman]
    end
  end
end
