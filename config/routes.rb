Rails.application.routes.draw do
  get 'welcome/index'

  namespace :api do
    namespace :v1 do
      get '/heroes/*publishers' => 'heroes#get_them', as: :heroes
    end
  end

  root 'welcome#index'
  mount Heroes::API => '/'
end
